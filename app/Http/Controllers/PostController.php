<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Post;

class PostController extends Controller
{

	public function __construct() {
		$this->middleware('auth');
	}

    public function index(){
    	$posts = Post::all();
    	return view('post', compact('posts'));
    }

    public function create(){
    	$categories = Category::all();
    	//dd($categories);
    	return view('createpost', compact('categories'));
    }

    public function store(Request $request) {
    	$request->validate([
    		'title' => 'required',
    		'content' => 'required|min:6|string',
    		]);
    	//dd(request('content'));
    	Post::create([
    		'title' => $request->title,
    		'category_id' => request('category'),
    		'content' => request('content'),
    		]);
    	return redirect()->route('post');
    }

    public function edit($id){
    	$post= Post::where('id',$id)->first();
    	$categories = Category::all();
    	return view('editpost', compact('post', 'categories'));
    }

    public function detail($id){
        $post= Post::where('id',$id)->first();
       // $category = Category::where('id', $post->category_id)->first();
        return view('detailpost', compact('post'));
    }

    public function update(Request $request, $id){
    	$post=Post::find($id)->first();
    	$request->validate([
    		'title' => 'required|string',
    		'content' => 'required|min:6|string',
    		]);

    		$post->update([
    			'title' => $request->title,
    			'category_id' => request('category'),
    			'content' => request('content'),
    		]);

    		return redirect()->route('post');
    }

    public function destroy($id){
    	$post = Post::find($id)->first();
    	$post->delete();

    	return redirect()->back();
    }
}
