@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        @foreach($posts as $post)
            <div class="card">
                <div class="card-header">{{$post->title}}
                  <a href="{{route('post.detail',$post->id)}}"  style="display:inline" class="btn btn-xs btn-primary">Read More</a></div>
                <div class="card-body">
                    {{str_limit($post->content,100,'...')}}
                    <br>
                        <a href="{{route('post.edit',$post->id)}}" style="display:inline" class="btn btn-xs btn-primary">Edit</a>
                        <a href="{{route('post.delete',$post->id)}}" onclick="return confirm('Are you sure want to delete this item?');" style="display:inline" class="btn btn-xs btn-danger" 
                            onclick="event.preventDefault();
                                    getElementById('post-delete').submit(); ">Hapus</a>
                </div>
            </div>
            <form action="{{route('post.delete',$post->id)}}" id="post-delete" method="post">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                </form>
            @endforeach
                
        </div>
    </div>
</div>
@endsection
