@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row ">
        <div class="col-md-8 col-md-offset-2">
           <div class="panel panel-default">
                <div class="panel-header">
                    {{$post->title}} | <small>{{$post->category->nama}}</small> | By : {{auth()->user()->name}} | {{$post->create}}
                 </div>
                <div class="panel-body">
                    {{$post->content}}
                </div>
                </div>
                @foreach($post->comment()->get() as $comment)
                 <div class="panel panel-default">
                 <div class="panel-heading">
                 {{$comment->user->name}} | {{$comment->created_at->diffForHumans()}}
                </div>
                <div class="panel-body">
                {{$comment->message}}
                </div>
                </div>
                @endforeach

                <form action="{{route('post.comment',$post->id)}}" method="POST">

                        @csrf
                <div class="card-body">
                    <input class="form-control" type="text" name="message" placeholder="Komentar">
                </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                
            
        
    </div>
</div>
@endsection
